package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Response struct {
	Status string `json:"status"`
}

func main() {
	port := 8080

	LoadConfig()

	router := gin.New()
	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, Response{"Alive"})
	})
	router.GET("/config", func(c *gin.Context) {
		c.JSON(http.StatusOK, GetAll())
	})
	router.Run(fmt.Sprintf(":%v", port))
}
