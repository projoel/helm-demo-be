package main

import (
	"github.com/kelseyhightower/envconfig"
)

// DatabaseConfig contains the necessary fields to connect to a SQL database server (specifically
// postgres based, but otherwise pretty generic)
type DatabaseConfig struct {
	Host     string `json:"host" required:"true"`
	Port     int    `json:"port" default:"5432"`
	Username string `json:"username" required:"true"`
	Password string `json:"-" required:"true"` // Hiding, because we don't want to share passwords
	Database string `json:"database" required:"true"`
}

type AppConfig struct {
	Version     string `json:"version" default:"v0.0.0-unknown"`
	Environment string `json:"environment" default:"unknown"`
}

//AllConfig is a larger structure that encompasses all of the configuration data -- used for
//echoing configuration during debugging input.
type AllConfig struct {
	Db  DatabaseConfig `json:"db"`
	App AppConfig      `json:"app"`
}

var (
	db  DatabaseConfig
	app AppConfig
	all AllConfig
)

// LoadConfig loads all of the environment configuration specified in environment variables
func LoadConfig() error {
	var err error

	funcs := []func() error{
		loadDbConfig,
		loadAppConfig,
	}

	for _, f := range funcs {
		err = f()
		if err != nil {
			break
		}
	}

	all.Db = db
	all.App = app

	return err
}

func loadDbConfig() error {
	config := DatabaseConfig{}
	err := envconfig.Process("DB", &config)
	db = config

	return err
}

func loadAppConfig() error {
	config := AppConfig{}
	err := envconfig.Process("APP", &config)
	app = config

	return err
}

// DbHost returns back the value of the env variable DB_PORT, which specifies where the
// database server is located
func DbHost() string {
	return db.Host
}

// DbPort returns back the value of the env variable DB_PORT, which specifies which port
// to use when connecting to the database server
func DbPort() int {
	return db.Port
}

// DbUsername returns back the value of the env variable DB_USERNAME, which specifies the
// username needed to connect to the database server
func DbUsername() string {
	return db.Username
}

// DbPassword returns back the value of the env variable DB_PASSWORD, which specifies the
// password needed to connect to the database server
func DbPassword() string {
	return db.Password
}

// DbDatabase returns back the value of the environment variable DB_DATABASE, which specifies
// which actual database to use on the database server
func DbDatabase() string {
	return db.Database
}

func Environment() string {
	return app.Environment
}

func AppVersion() string {
	return app.Version
}

// GetAll returns a copy of all of the configurations (that are not secret/private -- e.g. passwords)
func GetAll() AllConfig {
	return all
}
