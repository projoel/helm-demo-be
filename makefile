# import local .env file (Credit https://unix.stackexchange.com/questions/235223/makefile-include-env-file)
include .env
export $(shell sed 's/=.*//' .env)

# Go compiler configurations
ARCH=amd64
USE_CGO=0

APP_NAME=app
PROJECT_NAME=helm-demo-be
PRIMARY_CMD_PATH=cmd

.PHONY: clean
clean:
	mkdir -p dist
	rm -rf dist/*

.env:
	cp .env_template .env

# golang build support: https://gist.github.com/asukakenji/f15ba7e588ac42795f421b48b8aede63

.PHONY: for-linux
for-linux: 
	CGO_ENABLED=$(USE_CGO) GOOS=linux GOARCH=$(ARCH) go build -o dist/linux/$(APP_NAME) ${PRIMARY_CMD_PATH}/*.go

.PHONY: for-win
for-win: 
	CGO_ENABLED=$(USE_CGO) GOOS=windows GOARCH=$(ARCH) go build -o dist/win/$(APP_NAME).exe ${PRIMARY_CMD_PATH}/*.go

.PHONY: for-osx
for-osx: 
	CGO_ENABLED=$(USE_CGO) GOOS=darwin GOARCH=$(ARCH) go build -o dist/osx/$(APP_NAME) ${PRIMARY_CMD_PATH}/*.go

.PHONY: build
build: clean for-linux

# Not really needed here
# .PHONY: build-all
# build-all: build for-win for-osx

.PHONY: build-docker
build-docker: build
	sudo docker build -t $(PROJECT_NAME):latest .

.PHONY: run-docker
run-docker:
	sudo docker-compose build
	sudo docker-compose up

.PHONY: rerun-docker
rerun-docker: build run-docker

.PHONY: test
test:
	go test ./...

.PHONY: run
run:
	go run cmd/ms-ex/*.go

.PHONY: tree
tree:
	tree -a -I '.git|vendor'
