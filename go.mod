module gitlab.com/projoel/helm-backend-demo

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/kelseyhightower/envconfig v1.4.0
)
