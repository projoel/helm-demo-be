FROM alpine:3.7

WORKDIR /app

COPY dist/linux/app .

CMD ["./app", "-port=8080"]